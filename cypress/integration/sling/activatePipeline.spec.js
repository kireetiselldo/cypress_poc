describe('Create and activate lead pipeline',()=>{
    it('login to app',()=>{
        cy.login();
        cy.navigateToSetup();
        cy.navigateToLeadManagePipeline();
        cy.addPipeline();
        cy.activatePipeline();
    })    
})
