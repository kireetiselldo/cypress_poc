// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --

require('cypress-xpath')

Cypress.Commands.add("login", () => {

        cy.visit('http://app-qa.sling-dev.com/signIn')        
        cy.get('#input_email').type('test22@mailinator.com')
        cy.get('#input_password').type('Test@777')
        cy.get("button.btn").click();    

})

Cypress.Commands.add("navigateToSetup", () => {    
    cy.get('.header-bottom > .link-primary').click();
    cy.get('#product-control-center > .dummy-product').click();
    cy.xpath("//div[child::a[contains(text(),'Leads')]]/button").click();
    cy.xpath("//a[text()='Manage your Pipeline' and parent::div[@class='dropdown-menu show']]").click();
})

Cypress.Commands.add("navigateToSalesDashboard", () => {    
    cy.get('.header-bottom > .link-primary').click();
    cy.get(':nth-child(1) > .dummy-product').click();    
})

Cypress.Commands.add("navigateToleadsListingPage", () => {    
    cy.xpath("//a[text()='Leads' and parent::div[contains(@class,'btn-group')]]").click(); 
    cy.get('.btn-primary.mr-2').click();
    var ran = new Date().getTime();
    cy.get("div.validate input[id='12_input_firstName']").type('first')                             
    cy.get("div.validate input[id='13_input_lastName']").type('lasname'+ran);
    cy.get('.form-buttons > .btn-primary').click();   
})

Cypress.Commands.add("createLead", () => {               
})

Cypress.Commands.add("navigateToLeadManagePipeline", () => {        
    cy.xpath("//div[child::a[contains(text(),'Leads')]]/button").click();
    cy.xpath("//a[text()='Manage your Pipeline' and parent::div[@class='dropdown-menu show']]").click();
})

Cypress.Commands.add("addPipeline", () => {    
    cy.get('.btn-primary.mr-2').click();
    var ran = new Date().getTime();
    cy.xpath("//input[@name='name']").type('pipeline demo'+ran);
    cy.get('.form-buttons > .btn-primary').click();
    
})

Cypress.Commands.add("activatePipeline", () => {    
    cy.xpath("//button[text()='Activate']").click();
})

Cypress.Commands.add("createLead", () => {    
    cy.xpath("//button[text()='Activate']").click();
})




//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
